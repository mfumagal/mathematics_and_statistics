\babel@toc {UKenglish}{}
\beamer@sectionintoc {1}{Image classification}{4}{0}{1}
\beamer@sectionintoc {2}{Linear classification}{19}{0}{2}
\beamer@sectionintoc {3}{Optimisation}{39}{0}{3}
\beamer@sectionintoc {4}{Neural networks}{45}{0}{4}
\beamer@sectionintoc {5}{convnets}{61}{0}{5}
\beamer@sectionintoc {6}{keras}{75}{0}{6}
\beamer@sectionintoc {7}{Practical}{98}{0}{7}


Introduction to probability and statistics and deep learning for MSc Bioinformatics and Theoretical Systems Biology, as part of the _Mathematics and Statistics_ and _Bioinformatics II_ modules, at Imperial College London.

This repository includes schedule and all material in a series of slides and jupyter notebooks
This material should be used only by students from the aforementioned course.
These notes and slides have been mostly based on "Statistical Inference", Casella & Berger, 2nd Ed.; "Bayesian methods for Data Analysis", Carlin & Louis, 3rd Ed.; taught material from Professor M Stumpf; "Probabilistic graphical models", Koeller and Friedman.
Please do not redistribute or post it anywhere else.

Prerecorded videos are accessible by logging in into Panopto and select "Panopto > Browse > Life Sciences > LIFE97030 - Mathematics and Statistics > Fumagalli" 
Alternatively, [this link](https://imperial.cloud.panopto.eu/Panopto/Pages/Sessions/List.aspx#folderID=%222547da0e-dd7d-4a43-a436-ac4e00ba09e4%22) may also work.

## Instructions

You can use your laptop during lectures.

To clone the repository on your machine:
```
git clone https://bitbucket.org/mfumagal/mathematics_and_statistics
```
I suggest you make a fresh local copy that you can edit.

Follow instructions [here](https://irkernel.github.io/installation/) to install the R kernel for jupyter notebooks and [here](https://github.com/damianavila/RISE) to install RISE (if you wish so).
The following R packages are required: abc, coda, maps, spam, fields.

To open a notebook, type
```
jupyter-notebook
```
The first time you open a notebook, make sure you have a "clean" version by clicking on "restart and clear output" under the "Kernel" option.

Guidance on typing using markdown can be found [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
Likewise, a good source for latex formatting of mathematical objects is [here](https://en.wikibooks.org/wiki/LaTeX/Mathematics).


## Slides

Slides can be found in ```Slides``` folder and are called ```main.pdf``` in each subfolder.
I may use jupyter-notebooks instead of slides during lectures, but the content is very similar.


## Timetable 2020-2021

* 15/10 at 9-9.30
Introduction live on Teams

* 15/10 at 9.30-11	
Panopto recordings: 01_set_theory 02_set_operations 03_probability_function 04_counting

* 19/10 at 9-11
Panopto recordings: 05_conditional_probability 06_random_variables

* 20/10 at 13-14
Q&A live on Teams on recordings 01-06

* 22/10 13-15
Panopto recordings: 07_mass_density 08_discrete_probability 09_continuous_probability

* 26/10 10-11
Q&A live on Teams on recordings 07-09

* 26/10 14-16
Panopto recordings: 10_descriptive 11_estimators 12_likelihood 13_hypothesis_testing

* 27/10 12-13
Q&A live on Teams on recordings 10-13

* 27/10 14-16
Panopto recordings: 14_bayesian_statistics_partA 15_bayesian_statistics_partB

* 28/10 11-12
Q&A live on Teams on recordings 14-15

* 29/10 10-12
Problem class live on Teams

* 10/12 14-16
Panopto recordings: Fumagalli_deep_learning (for Bioiformatics II)

* 11/12 11-12
Q&A live on Teams (for Bioinformatics II on deep learning)

* 14/12 11-12
Assignment feedback live on Teams


















### A

# calculate the Likelihood for each possible value of theta
p <- seq(0, 1, 0.01)
L <- dbinom(x=40,prob=p,size=200)

plot(p, L, type="l")

# MLE
p[which.max(L)]


### B

# likelihood ratio
LR <- -2*(log(L[2])-log(max(L)))

# p-value
1-pchisq(LR,df=1)


### C

# beta-binomial model of allele frequencies
p <- seq(0, 1, 0.01)
k <- 40
n <- 200
alpha <- k+1
beta <- n-k+1

y <- dbeta(p, shape1=alpha, shape2=beta)

plot(x=p, y=y, ylab="Posterior density" , xlab="Population frequency of G", type="l")

# notable quantiles
qbeta(p=c(0.025,0.25,0.5,0.75,0.975), shape1=alpha, shape2=beta)

# with only 10 individuals
k <- 4
n <- 20

alpha <- k+1
beta <- n-k+1

y <- dbeta(p, shape1=alpha, shape2=beta)

plot(x=p, y=y, ylab="Posterior density" , xlab="Population frequency of G", type="l")

qbeta(p=c(0.025,0.25,0.5,0.75,0.975), shape1=alpha, shape2=beta)


### D

# with informative prior

p <- seq(0, 1, 0.01)

k <- 40
n <- 200
alpha <- k+0.5
beta <- n-k+0.5
y <- dbeta(p, shape1=alpha, shape2=beta)
plot(x=p, y=y, ylab="Posterior density" , xlab="Population frequency of G", type="l")
qbeta(p=c(0.025,0.25,0.5,0.75,0.975), shape1=alpha, shape2=beta)

k <- 4
n <- 20
alpha <- k+0.5
beta <- n-k+0.5
y <- dbeta(p, shape1=alpha, shape2=beta)
points(x=p, y=y, type="l", lty=2)
qbeta(p=c(0.025,0.25,0.5,0.75,0.975), shape1=alpha, shape2=beta)

### E

# with many samples

k <- 40
n <- 200
alpha <- k+0.5
beta <- n-k+0.5

# posterior of M1 and M2
M1 <- pbeta(0.5, shape1=alpha, shape2=beta)
M2 <- (1-pbeta(0.5, shape1=alpha, shape2=beta))
BF <- M1/M2
BF

# with less samples

k <- 4
n <- 20
alpha <- k+0.5
beta <- n-k+0.5

M1 <- pbeta(0.5, shape1=alpha, shape2=beta)
M2 <- (1-pbeta(0.5, shape1=alpha, shape2=beta))
BF <- M1/M2
BF



